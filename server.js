var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var session = require('express-session');
var flash = require('connect-flash');
var initPassport = require('./app/config/passport/init');

var router = require('./app/routes/routes');
var db = require('./app/config/db/db');

var app = express();

app.use(session({secret: '<mysecret>', 
                 saveUninitialized: true,
                 resave: true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json()); // parse application/json 
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded
app.use(express.static(__dirname + '/public'));
app.use('/', router);

initPassport(passport);
mongoose.connect(db.url); // connect to database

app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/index.html'));
    });

app.set('port',process.env.PORT || 4352);
app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
})