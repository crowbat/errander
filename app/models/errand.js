var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ErrandSchema = new Schema({
    username: String,
    title: String,
    desc: String,
    create_time: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Errand', ErrandSchema);