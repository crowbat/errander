var express = require('express');
var passport = require('passport');
var Errand  = require('../models/errand');

var router = express.Router();
var a = 0
router.route('/errand')
    .get( function(req, res) {
        Errand.find(function(err, errands) {
            if (err)
                res.send(err);
            res.json(errands);
        });
    })
    
    .post(function(req, res) {
        var errand = new Errand();
        errand.username = req.user.username;
        errand.title = req.body.title;
        errand.desc = req.body.desc;
        errand.save(function(err) {
            if (err) {
                console.log(err);
            }
        });
        res.end();
    });

router.route('/errand/:errand_id')
    .get(function(req, res) {
        Errand.findById(req.params.errand_id, function(err, errand) {
            if (err) {
                console.log(err);
            }
            res.json(errand)
        });
    })
    
    .delete(function(req, res) {
        Errand.remove({
            _id: req.params.errand_id
        }, function(err) {
            if (err) {
                console.log(err);
             }
        });
    });

router.post('/signup', passport.authenticate('signup', {
    successRedirect: '/',
    failureRedirect: '/signup',
    failureFlash : true 
}));
    
router.post('/login', passport.authenticate('login', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash : true  
}));

router.get('/logout', function(req, res) {
    req.logout();
    req.session.destroy();
    res.json(false);
});

router.get('/is_logged_in', function(req, res) {
    if (req.isAuthenticated()) {
        a+= 1
        res.json(true);
    } else {
        res.json(false);
    }
});

router.get('/username', function(req, res) {
    if (req.isAuthenticated()) {
        res.json(req.user.username);
    } else {
        res.json(false);
   }
});

var isAuthenticated = function (req, res, next) {
  if (req.isAuthenticated())
    return next();
  res.redirect('/');
}
module.exports = router