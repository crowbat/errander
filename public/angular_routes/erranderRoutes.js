angular.module('erranderRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
    
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })
        
        .when('/new_errand', {
            templateUrl: 'views/new_errand.html',
            controller:'NewErrandController'
        })
        
        .when('/view_errand/:errand_id', {
            templateUrl: 'views/view_errand.html',
            controller:'ViewErrandController'
        })
        
        .when('/signup', {
            templateUrl: 'views/signup.html',
            controller:'SignUpController'
        })
        
        .when('/login', {
            templateUrl: 'views/login.html',
            controller:'LogInController'
        })
        
        .when('/profile', {
            templateUrl: 'views/profile.html',
            controller:'ProfileController'
        })
        
    $locationProvider.html5Mode(true);
}]);