angular.module('errandService', [])
    .factory('Errands', function($http) {
        return {
            get: function(id) {
                return $http.get('/errand/' + id);
            },
            create : function(errandData) {
                return $http.post('/errand', errandData);
            },
            delete : function(id) {
                return $http.delete('/errand/' + id);
            }
        }
    });