var mainctrl = angular.module('MainCtrl', [])

mainctrl.controller('MainController', function($rootScope, $scope, $http, $location, Errands) {

    Errands.get('')
        .success(function(data) {
            $scope.errands = data;
        })
        
    $scope.logout = function() {
        $http.get('/logout')
            .success(function(data) {
                $rootScope.logged_in = false;
                console.log('logging out');
                console.log(data);
                $location.path('/'); 
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
});

mainctrl.controller('NewErrandController', function($scope, $http, $location, Errands) {

    $http.get('/is_logged_in')
        .success(function(data) {
            if (!data) {
                $location.path('/');
            }
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
        
    $scope.formData = {};
    $scope.createErrand = function() {
        if ($scope.formData) {
            Errands.create($scope.formData)
                .success(function(data) {
                    $scope.formData = {};
                })
            $location.path('/');    
        }
    };
});

mainctrl.controller('ViewErrandController', function($scope, $http, $routeParams, $location, Errands) {
    Errands.get($routeParams.errand_id)
        .success(function(data) {
            $scope.errand = data;
            $http.get('/username')
                .success(function(data) {
                    if (data) {
                        $scope.can_delete = (data == $scope.errand.username || data == 'admin')
                    } else {
                        $scope.can_delete = false;
                    }
                });
        })
        
    $scope.deleteErrand = function() {
        Errands.delete($routeParams.errand_id)
            .success(function(data) {
                console.log('Success');
            })
        $location.path('/');
    };
});

mainctrl.controller('SignUpController', function($rootScope, $scope, $http, $location) {
    $scope.formData = {};
    $scope.signup = function() {
        $http.post('/signup', $scope.formData)
            .success(function(data) {
                $http.get('/is_logged_in')
                    .success(function(data) {
                        $rootScope.logged_in = data;
                        console.log('root scope is ' + $rootScope.logged_in);
                    })
                    .error(function(data) {
                        console.log('Error: ' + data);
                    });
                $location.path('/');
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
});

mainctrl.controller('LogInController', function($rootScope, $scope, $http, $location) {
    $scope.formData = {};
    $scope.login = function() {
        $http.post('/login', $scope.formData)
            .success(function(data) {
                console.log('here')
                $http.get('/is_logged_in')
                    .success(function(data) {
                        $rootScope.logged_in = data;
                        if (!data) {
                            $location.path('/login');
                        } else {
                            $location.path('/');
                        }
                    })
                    .error(function(data) {
                        console.log('Error: ' + data);
                    });
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };
});

mainctrl.controller('ProfileController', function($http, $location) {
    $http.get('/is_logged_in')
        .success(function(data) {
            if (!data) {
                console.log('Not logged in');
                $location.path('/');
            }
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });
});